# Parton Shower tutorial

In this tutorial we will discuss the construction of a parton shower, the implementation of on-the-fly uncertainty estimates, and of matrix-element corrections, and matching at next-to-leading order. At the end, you will be able to run your own parton shower for e+e- to hadrons at LEP energies and compare its predictions to results from the event generator Sherpa (using a simplified setup). You will also have constructed your first MC@NLO and POWHEG generator.

## Getting started

The Docker container for this tutorial can be pulled directly from [dockerhub](https://hub.docker.com/).
```
  docker pull cteqschool/tutorial:ps
```
Should you have problems with disk space, consider running docker containers prune and docker system prune first. To launch the docker container, use the following command
```
  docker run -it -u $(id -u $USER) --rm -v $HOME:$HOME -w $PWD cteqschool/tutorial:ps
```
You can also use your own PC (In this case you should have PyPy and Rivet installed). Download the tutorial and change to the relevant directory by running
```
  git clone https://gitlab.com/cteq-tutorials/2022.git tutorials && cd tutorials/ps/
```

## Instructions

For the remainder of the tutorial, follow the instructions in the [Worksheet](worksheet.pdf)


