# Resummation tutorial

In this tutorial we will discuss the construction of leading logarithmic resummation for simple additive observables in e+e- to hadrons such as thrust, fractional energy correlation, etc. At the end, you will have gained an understanding of the connection between the analytic approach to resummation and a toy parton shower.

## Getting started

The Docker container for this tutorial can be pulled directly from [dockerhub](https://hub.docker.com/).
```
  docker pull cteqschool/tutorial:ps
```
Should you have problems with disk space, consider running docker containers prune and docker system prune first. To launch the docker container, use the following command
```
  docker run -it -u $(id -u $USER) --rm -v $HOME:$HOME -w $PWD cteqschool/tutorial:ps
```
You can also use your own PC (In this case you should have PyPy and Rivet installed). Download the tutorial and change to the relevant directory by running
```
  git clone https://gitlab.com/cteq-tutorials/2022.git tutorials && cd tutorials/ll/
```

## Instructions

For the remainder of the tutorial, follow the instructions in the [Worksheet](worksheet.pdf)
