# Pythia Tutorial

Contact: Leif Gellersen and Phil Ilten

## Getting started
You can download the docker container for the tutorial using this command
```
  docker pull pythia8/tutorials:cteq22
```
To start up the tutorial, simply run
```
  docker run --rm -u `id -u $USER` -v $PWD:$PWD -w $PWD \
    -p 8888:8888 -it pythia8/tutorials:cteq22
```
This will start a Jupyter notebook server. Please follow the instructions in worksheet8307.ipynb. This worksheet is based on Pythia's Python interface. 

## There's more
If you'd like to work with C++, you can either follow the instructions in worksheet8300.pdf to install Pythia locally, or run the container interactively with 
```
  docker run --rm -u `id -u $USER` -v $PWD:$PWD -w $PWD \
    -p 8888:8888 -it pythia8/tutorials:cteq22 bash
```
to use the Pythia installation within the container.
