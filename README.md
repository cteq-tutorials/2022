# MC and ML tutorials at the 2022 CTEQ school

## Getting started

These tutorials use containers. Please install [Docker](http://docker.com) on your personal computer prior to the tutorial. If you have questions on installing Docker or on testing the Docker environment, please ask the lecturers before the tutorial. Due to time constraints we cannot assist everyone with setting up Docker during the tutorial itself.

## Running the tutorials

Instructions for the tutorials are found in the subdirectories of this repository.
- [Herwig tutorial](herwig)
- [Pythia tutorial](pythia)
- [Sherpa tutorial](sherpa)
- [Resummation tutorial](ll)
- [Parton-shower tutorial](ps)
- [Machine Learning tutorial](ml)

Please refer to the readme files and worksheets in in the individual directories for instructions.
