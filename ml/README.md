# Machine Learning tutorial

## Download
**NOTE**: The setup and download can take some time. Please handle this before the start of the tutorials.

The Docker container can be pulled directly from [dockerhub](https://hub.docker.com/). The corresponding command line is
```
   docker pull jupyter/tensorflow-notebook
```

The data to be used for training the neural network can be downloaded on the command line using
```
   wget https://zenodo.org/record/2603256/files/test.h5
   wget https://zenodo.org/record/2603256/files/train.h5
   wget https://zenodo.org/record/2603256/files/val.h5
```

## Tutorial Overview

## Starting the Tutorial 

The tutorial notebook can be opened by running
```
   docker run -it -p 8888:8888 -v ${PWD}:/home/jovyan/work jupyter/tensorflow-notebook 
```
then copying and pasting the jupyter notebook link into your internet browser and finally navigating to `work/tutorial.ipynb`.

## Additional References
