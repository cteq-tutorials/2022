# Sherpa tutorial at the 2022 CTEQ school

## Getting started

This tutorial uses a container. Please install [Docker](http://docker.com) on your personal computer prior to the tutorial. If you have questions on installing Docker or on testing the Docker environment, please ask the lecturers before the tutorial. Due to time constraints we cannot assist everyone with setting up Docker during the tutorial itself.

## Download

The Docker container can be pulled directly from [dockerhub](https://hub.docker.com/). The corresponding command line is
```
  docker pull cteqschool/tutorial:sherpa-2.2.7
```

## Running the container

We recommend to run the container with your home directory mounted and using your actual userid. For any custom rivet analysis to work you will also need to set the RIVET_ANALYSIS_PATH environment variable. In order to simplify the docker call you may define an alias. 
```
  alias docker-run-sherpa='docker run -it -u $(id -u $USER) -v $HOME:$HOME \
    -w $PWD --env="RIVET_ANALYSIS_PATH=." cteqschool/tutorial:sherpa-2.2.7'
```
You may consider adding the --rm flag to automatically clean up after your container exits. Note that in this case all modifications you may have made to the container will be lost.

## Instructions

For instructions, please refer to the file [sherpa.pdf](sherpa.pdf) in this directory
